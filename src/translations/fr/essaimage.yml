title: Essaimage
intro: |-
  ## Transmettre les savoir-faire

  Bienvenue dans un monde où le cloud éthique devient à la portée de tou·tes !

  @:html.soft désire favoriser des actions qui encouragent
  l’autonomie numérique, pour mettre à la portée du plus grand nombre un
  hébergement
  de confiance solidaire de nos vies numériques.

  Un processus d’essaimage déjà entamé et que l’on doit affiner pour proposer
  une alternative concrète et locale aux entreprises-silos moissonnant nos
  données.
sections:
  chatons: |-
    ### Des @:html.chatons pour un hébergement local, éthique et solidaire

    Né en octobre 2016 à l’initiative de @:html.soft, le Collectif des
    Hébergeurs
    Alternatifs, Transparents, Ouverts, Neutres et Solidaires est à la fois un
    label auto-géré et un outil d’entraide.

    Pour le public, ce label @:html.chatons sert à pouvoir identifier près de
    chez soi un hébergeur à qui confier ses données, à qui faire confiance.
    Cette
    confiance repose sur des engagements stricts, autour de valeurs proclamées
    telles que l’ouverture, la transparence, la neutralité et le respect des
    données
    personnelles.

    Pour les membres du collectif, @:html.chatons est un outil commun où l’on
    s’entraide, entre autres, par le partage des savoirs juridiques, techniques
    et relationnels qui permettent de rester dignes de cette confiance accordée
    par les bénéficiaires des services.

    @:html.soft veut pouvoir prendre le temps de se mettre à disposition
    du collectif. C’est ce dernier qui décidera des actions à mener ensemble
    pour
    se développer. Ceci dans l’espoir d’arriver à ce que, un jour, chacun·e
    puisse
    trouver un CHATON près de chez soi !
  yunohost: |-
    ### YUNOHOST, l’auto hébergement facile :white_check_mark:

    YUNOHOST est un projet libre ayant pour but de permettre à quiconque
    d’auto-héberger
    ses services avec un minimum de connaissances techniques. Emails, partage
    de fichiers, agendas, outils de création et d’organisation collaborative :
    tout cela s’installe en quelques clics afin d’avoir chez soi le serveur (et
    les données) de ses proches, son association, son entreprise, son collectif…

    Depuis janvier 2017, @:html.soft consacre du temps salarié au développement
    de ce projet afin de s’assurer qu’un maximum de services libres présentés
    dans la campagne « @:html.dio » puisse être disponible dans la solution
    YUNOHOST.

    C’est dans l’esprit de cette collaboration avec YUNOHOST que @:html.soft
    souhaite accompagner et promouvoir cette solution afin qu’elle remporte une
    adhésion massive…

    **Mise à jour oct. 2019** : *Mission accomplie ::white_check_mark: ! La
    majorité des services [@:color.dio](@:link.dio) sont [disponibles sur
    YUNOHOST](https://forum.yunohost.org/t/applications-framasoft/1680).*
  i18n: |-
    ### Internationalisation : partager l’expérience hors des frontières

    Avec la campagne « @:html.dio », @:html.soft propose plus de trente
    services web éthiques et alternatifs à un public francophone. Il est hors
    de propos de traduire les services hébergés par @:html.soft : cela ferait
    beaucoup trop de monde et donc de poids sur les épaules d’une petite
    association
    française !

    Néanmoins, parce qu’il propose une solution (presque) complète, une
    expérience
    (relativement) aboutie et qu’il a remporté une adhésion (modestement)
    remarquable,
    le projet @:html.dio semble quasiment unique au monde… et cela pourrait
    changer !

    Il y a là un magnifique chantier à défricher ensemble, celui de partager ces
    années de <i>dégooglisation</i> pour que d’autres puissent s’en inspirer et
    l’appliquer dans leur langue, en l’adaptant à leur propre culture.

    Bref, il s’agit juste de transformer, ensemble, l’histoire de « @:html.dio »
    en un commun international. Rien que ça !
  woc: |-
    ### Framasoft <i lang="en">Winter of code : winter is coding</i>

    Avec le <i lang="en">Google Summer of code</i>, le géant du web est bien
    malin : financer les projets <i lang="en">open source</i> de développeurs
    et développeuses lui permet à la fois de choisir les codes qui vont dans son
    intérêt, mais aussi de séduire des talents, de les formater à sa culture
    d’entreprise
    tout en redorant son blason !

    En outre, le monde des libertés numériques devient de plus en plus dépendant
    des contributions financées par <i>Google</i> et autres géants du Web pour
    perdurer, et c’est inquiétant.

    En proposant le <i lang="en">Winter of code</i>, @:html.soft désire faire
    sa part dans l’inversion de cette tendance. L’idée maîtresse est de mettre
    en relation les communautés de logiciels libres ayant besoin de talents et
    les stagiaires des métiers numériques cherchant une formation qui ait du
    sens,
    et d’apporter un soutien administratif, voire financier.
    Tout ceci reste à imaginer avec les parties concernées, mais une chose reste
    sûre : <i lang="en">Winter is coding</i> !

