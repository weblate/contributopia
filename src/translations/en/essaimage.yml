title: Hiving off
intro: |-
  Welcome to a world where the ethical cloud is in reach of each and every one
  of us!
  For 2018 and 2019, @:html.soft will prioritise actions which encourage digital
  autonomy, in order to offer to as many as possible a solidarity hosting of our
  digital life that is trustworthy.
  A process of hiving off which has already started and which needs refining in
  order to offer real local alternatives to the enterprise-silos which harvest
  our data.
sections:
  chatons: |-
    ### @:html.chatons for local, ethical, solidarity hosting'

    Inaugurated in October 2016 on the initiative of @:html.soft, the Collective
    of Keen Internet Talented Teams Engaged in Network Services is both a
    self-managed
    label and a space for mutual aid.

    For the public, the @:html.chatons label indicates a local host in whom you
    can trust your data with confidence. This confidence is based on a number
    of strictly followed commitments that come from values like openness,
    transparency,
    neutrality and respecting personal data.

    For the members of the collective, @:html.chatons is a shared tool for
    mutual
    support through, amongst other things, the sharing of the legal, technical
    and interpersonal knowledge necessary to be worthy of the trust invested by
    the beneficiaries of the services.

    @:html.soft will take the time to listen to and respond to the needs
    of the collective. It is the latter who will decide the course of action in
    order to grow. This is done in the hope that one day each and every one of
    us will be able to find a KITTEN in our neighbourhood!
  yunohost: |-
    ### YUNOHOST, easy self hosting

    YUNOHOST is a free software project which aims to give access to even the
    least tech savvy of us to the self hosting of services. Emails, file
    sharing,
    agendas, tools for collaborative creation and organisation: and all that
    within
    reach of a few clicks in order to have your own server (and the data) at
    home
    for you, your friends and family, your organisation, your business, your
    collective…

    Since January 2017, @:html.soft has been investing its employees worktime
    in order to be sure that there can be a maximum of the libre services from
    the “@:html.dio” campaign available in the YUNOHOST solution.

    It is in this spirit of collaboration with YUNOHOST that @:html.soft
    is accompanying and promoting this solution in order that it will be adopted
    on a massive scale.
  i18n: |-
    ### Internationalisation: sharing our experience beyond our borders'

    With the “@:html.dio” campaign, @:html.soft is offering more than 30
    ethical and alternative web services to a French speaking public. It is out
    of the question to translate the services hosted by @:html.soft: that
    would result in too many users and too much weight on the shoulders of a
    small
    French association!

    Nevertheless, because the campaign offers a solution that is (more or less)
    complete, an experience (relatively) perfected and because it has resulted
    in a participation that is (modestly) remarkable, the @:html.dio project
    seems
    to be more or less unique in the world… and that could change!

    There is a vast work of construction here for which the foundations need
    laying,
    that of sharing the years of <i>de-google-ification</i> so that others can
    take inspiration and apply it in their language, adapting it to their
    culture.

    In effect, it means just transforming, together, the history of ”@:html.dio”
    into an international commons. Nothing more than that!
  woc: |-
    ### Framasoft <i lang="en">Winter of code: winter is coding</i>'

    With the <i lang="en">Google Summer of code</i>, the web giant is indeed
    crafty: financing the <i lang="en">open source</i> projects of developers
    allows <i>Google</i> to handpick the codes that will be of benefit, and at
    the same time to seduce talented coders, to format them into its business
    culture, all the while waving the <i>Google</i> banner.

    In addition, the world of digital freedoms is becoming more and more
    dependent
    on the financial contributions of <i>Google</i> and the other web giants in
    order to survive and it is becoming worrying.

    By proposing the <i lang="en">Winter of code</i>, @:html.soft wishes
    to do its bit to invert this tendency. The heart of the idea is to put free
    software communities in contact with trainees in information technology
    seeking
    experience in an environment that makes sense, @:html.soft will bring
    administrative and perhaps even financial support.

    All that remains to be imagined with the interested parties but one thing
    is sure: <i lang="en">Winter is coding</i>!

